using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotator : MonoBehaviour
{
    public float rotateSpeed;

    private void Update()
    {
        //if (Input.GetMouseButton(0))
        //{
        //    Debug.Log(Input.GetAxis("Mouse X"));
        //    float rotX = Input.GetAxis("Mouse X") * rotateSpeed * Mathf.Deg2Rad;
        //    transform.Rotate(Vector3.up, -rotX);
        //}

        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Moved)
            {
                Quaternion rot = Quaternion.Euler(0f, -touch.deltaPosition.x * rotateSpeed, 0f);
                transform.rotation = rot * transform.rotation;
            }
        }
    }
}
