using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class TriggerController : MonoBehaviour
{
    [System.Serializable]
    public struct TriggerOption
    {
        public Transform cameraPoint;
        public GameObject mesh;
        public string title;
        public string description;
    }

    public LayerMask masking;
    public Transform camTransform;
    public Transform objectTransform;
    public Transform arrowTransform;
    public Transform mainObject;
    public List<Transform> optionsTransform;
    public List<TriggerOption> options;
    public GameObject activeSprite;
    public GameObject inactiveSprite;
    public GameObject componentInfo;
    public Text titleText;
    public Text descriptionText;


    private Transform optionSelected;
    private bool isTriggered = false;

    public void SetObjectTransform(Transform objectTransform)
    {
        this.objectTransform = objectTransform;
    }

    // Start is called before the first frame update
    void Start()
    {
        componentInfo.SetActive(false);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //if (!isTriggered) return;
        
        RaycastHit2D hit = Physics2D.Raycast(arrowTransform.GetChild(0).position, arrowTransform.GetChild(0).up, 5, masking);
        var opt = options[0];
        if (hit && optionsTransform.Contains(hit.transform))
        {
            if (optionSelected == null || optionSelected != hit.transform)
            {
                optionSelected = hit.transform;
                opt = options[optionsTransform.IndexOf(optionSelected)];
                camTransform.DOMove(opt.cameraPoint.position, 1f);
                camTransform.DORotate(opt.cameraPoint.rotation.eulerAngles, 1f);
                titleText.text = opt.title;
                descriptionText.text = opt.description;
            }
        }

        if (optionSelected != null)
        {
            opt = options[optionsTransform.IndexOf(optionSelected)];
            opt.mesh.GetComponent<Animator>().Play("highlight");
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isTriggered = true;
        //inactiveSprite.SetActive(false);
        //activeSprite.SetActive(true);
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("objects"))
        {
            if (Vector2.Distance(this.transform.position, collision.transform.position) < 0.5f)
            {
                objectTransform = collision.transform;
                arrowTransform.rotation = Quaternion.Lerp(arrowTransform.rotation, objectTransform.rotation, .1f);
                inactiveSprite.SetActive(false);
                activeSprite.SetActive(true);

                if (optionSelected != null)
                {
                    componentInfo.SetActive(true);
                }
            }
        }
        else
        {
            inactiveSprite.SetActive(true);
            activeSprite.SetActive(false);
            componentInfo.SetActive(false);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isTriggered = false;
        //inactiveSprite.SetActive(true);
        //activeSprite.SetActive(false);
    }

    public Transform GetOptionSelected()
    {
        return optionSelected;
    }
}
